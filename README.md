
# Comtrade DB

This package facilitates creating a SQL database out of UN
Comtrade Data. It leverages the functions made available by the
[`etl`](https://github.com/beanumber/etl) package.

# Status

Currently, only for testing purposes usable:

```r
library(comtradedb)
library(tidyverse)

obj <- etl("comtradedb", dir = "dump")
## --------------------
## Now, copy the raw UN Comtrade files (in fst format) to dump/raw!
## This step will be automated/implemented
## --------------------
obj %>% etl_create()

## obj %>% etl_init() %>%
##   etl_extract() %>%
##   etl_transform() %>%
##   etl_load()

obj %>% tbl("trade") -> trade

trade %>%
  filter(rep == "AUT" & par == "KOR" & hscode == "030380") %>%
  collect() -> xx
str(xx)
xx

xx %>%
  group_by(rep, par, hscode, tradeflow) %>%
  fill(year, value, netweightkg)


trade %>% summarise(distinct(tradeflow))

trade %>% summarise(nr.hscodes = n_distinct(hscode))
```
