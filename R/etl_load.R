#' Load the UN Comtrade data
#' @import etl
#' @import fst
#' @inheritParams etl::etl_load
#' @export
#' @examples
#' \dontrun{
#' if (require(dplyr)) {
#'   obj <- etl("comtradedb") %>%
#'     etl_create()
#' }
#' }
etl_load.etl_comtradedb <- function(obj, ...) {
  smart_upload_fst(obj, ...)

  ## Always return obj invisibly to ensure pipeability!
  invisible(obj)
}


#' Upload a list of fst files to the DB
#' @param obj An \code{\link{etl}} object
#' @param src a list of fst files to upload. If \code{NULL}, will return all
#' fst in the load directory
#' @param ... arguments passed to \code{\link[DBI]{dbWriteTable}}
#' @importFrom DBI dbWriteTable
#' @importFrom magrittr %>%
#' @export
#' @examples
#' \dontrun{
#' if (require(RMySQL)) {
#'   # must have pre-existing database "fec"
#'   # if not, try
#'   system("mysql -e 'CREATE DATABASE IF NOT EXISTS fec;'")
#'   db <- src_mysql_cnf(dbname = "mtcars")
#' }
#' }
smart_upload_fst <- function(obj, src = NULL,
                             ## tablenames = NULL,
                             ...) {
  if (is.null(src)) {
    src <- list.files(attr(obj, "load_dir"),
                      pattern = "\\.fst", full.names = TRUE)
  }
  ## if (is.null(tablenames)) {
  ##   tablenames <- basename(src) %>%
  ##     gsub("\\.fst", "", x = .)
  ## }
  ## if (length(src) != length(tablenames)) {
  ##   warning("src and tablenames must be of the same length")
  ## }

  message(paste("Loading", length(src), "file(s) into the database"),
          appendLF = FALSE)
  ## write the tables directly to the DB
  ## mapply(DBI::dbWriteTable, name = tablenames, value = src,
  ## MoreArgs = list(conn = obj$con, append = TRUE, ... = ...))
  for(i in seq_len(length(src))) {
    message(".", appendLF = FALSE)
    DBI::dbWriteTable(conn = obj$con, name = "trade", #tablenames[i],
                      value = fst::read_fst(src[i]),
                      append = TRUE, ...)
  }
  message("")

  invisible(obj)
}
